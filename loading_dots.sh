#!/bin/bash

loadingtext=$1
echo -ne "$loadingtext\r"
while :
do
  echo -ne "$loadingtext.\r"
  sleep 0.5 
  echo -ne "$loadingtext..\r"
  sleep 0.5 
  echo -ne "$loadingtext...\r"
  sleep 0.5 
  echo -ne "\r\033[K"
  echo -ne "$loadingtext\r"
  sleep 0.5 
done
