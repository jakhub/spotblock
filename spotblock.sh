#!/usr/bin/env bash


function start_spot () {
  (spotify >/dev/null 2>/tmp/spotify_errors.log &)
  
  # wait for spotify to be ready

  sleep 3
  
  # restart spotify
  if [ $1 -eq 0 ]; then
    bash ./sp.sh "prev" 
    sleep 0.1
  fi
  bash ./sp.sh "pause"
  bash ./sp.sh "play"
}

function sync () {
  # TODO kill sync if failure
  (bash loading_dots.sh "Syncing" 2>/dev/null &)
  while [ "$title" == "$(bash ./sp.sh metadata | grep 'title' | sed 's/^title|//')" ] && [ "$skip" -eq 0 ] 
  do
    sleep 0.01 
  done
  pkill -f loading_dots.sh >/dev/null 2>&1
  echo -ne "\r\033[K"

  skip=0
}


function seconds2time () {
   T=$1
   H=$((T/60/60%24))
   M=$((T/60%60))
   S=$((T%60))

   printf '%02d:%02d:%02d\n' $H $M $S
}

function currently_playing {
  local prompt="> "
  elapsed_time=0
  while :
  do
    start_time="$(date -u +%s)"
    read -p "$prompt" -t $sleep_til input
    result="$?"
    end_time="$(date -u +%s)"
    elapsed_time="$(($end_time-$start_time))"
    
    if [ "$result" == "0" ]; then
      if [ "$input" == "set" ]; then
        skip=1
        break
      else
        bash ./sp.sh "$input"
        
        if [ "$input" == "pause" ]; then
          read -p "$prompt"
          
          bash ./sp.sh "play"
          let "sleep_til = $sleep_til - $elapsed_time"
        else
          skip=1
          break
        fi
      fi

    else
      break
    fi
  done
}

start_spot 0

title="-1"
skip=0
block_list="block_list.txt"
SPOTIFY="spotify"

while :
do
  sync

  sleep_til="$(bash ./sp.sh metadata | grep 'length' | sed 's/^length|//' | grep -oE '^[0-9]{3}')"
  title="$(bash ./sp.sh metadata | grep 'title' | sed 's/^title|//')"

  echo -n "$title --- "
  seconds2time $sleep_til
  
  if grep -Fxq "$title" "$block_list"; then
    killall spotify
    sleep 2
    start_spot 1
  else
    currently_playing
  fi

  if ! pgrep -x "$SPOTIFY" >/dev/null; then
    exit
  fi
done

